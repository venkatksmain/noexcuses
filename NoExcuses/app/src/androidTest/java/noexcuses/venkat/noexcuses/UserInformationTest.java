package noexcuses.venkat.noexcuses;

import android.app.Application;
import android.content.Context;
import android.test.ApplicationTestCase;

import junit.framework.Assert;

import java.io.OutputStreamWriter;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class UserInformationTest extends ApplicationTestCase<Application> {
    private UserInformation info;

    public UserInformationTest() {
        super(Application.class);
    }

    protected void setUp() {
        info = new UserInformation("Test User", "25", "125", "110", 5);
    }

    public void testSetUserName() {
        info.setName("Jim Kirk");
        Assert.assertEquals("Jim Kirk", info.getName());
    }

    public void testSetUserWeight() {
        info.setCurrentWeight("130");
        Assert.assertEquals("130", info.getCurrentWeight());
    }

    public void testEqualsTrue() {
        UserInformation other = new UserInformation("Test User", "25", "125", "110", 5);
        Assert.assertEquals(info, other);
    }

    public void testSaveAndLoadFile() {
        Context c = getContext();
        String user = "TestUser";
        info.saveToFile(c, user);
        UserInformation loadedInfo = UserInformation.readFromFile(c, user);
        Assert.assertEquals(info, loadedInfo);
    }
}