package noexcuses.venkat.noexcuses;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by Venkat on 3/27/15.
 */
public class UserInformation implements java.io.Serializable {

    private String name;
    private String age;
    private String currentWeight;
    private String targetWeight;
    private long concentration;   //1: cardio (weightloss), 2: cardio (maintenance), 3: powerlifting, 4: str training, 5:bodybuilding

    public UserInformation(String name, String age, String currentWeight, String targetWeight, long concentration){
        this.name = name;
        this.age = age;
        this.currentWeight = currentWeight;
        this.targetWeight = targetWeight;
        this.concentration = concentration;
    }

    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof UserInformation)) return false;

        UserInformation other = (UserInformation) o;
        return this.name.equals(other.name) &&
                this.age.equals(other.age) &&
                this.currentWeight.equals(other.currentWeight) &&
                this.targetWeight.equals(other.targetWeight) &&
                this.concentration == other.concentration;
    }

    /**
     * Reads in user information from file given the ApplicationContext and name of the active user.
     *
     * @param context The application context
     * @param activeUser The username of the active user
     * @return The UserInformation object for the active user
     */
    public static UserInformation readFromFile(Context context, String activeUser){
        File yourFile = new File(context.getFilesDir().getPath().toString() + "/" + activeUser + "/UserInformation.txt");
        try {
            System.out.println(yourFile.getAbsolutePath());
            FileInputStream f_in = new
                    FileInputStream(yourFile);

            ObjectInputStream obj_in =
                    new ObjectInputStream (f_in);


            Object obj = obj_in.readObject();

            if (obj instanceof UserInformation)
            {
                // Cast object to a Vector
                UserInformation vec = (UserInformation) obj;
                return vec;
            } else {
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Saves the UserInformation to a file in internal storage.
     * @param context The application context
     * @param activeUser The username of the active user
     */
    public void saveToFile(Context context, String activeUser){
        // Make sure we have created a directory for the activeUser
        File path = new File(context.getFilesDir(), "/" + activeUser);
        path.mkdirs();

        File newFile = new File(path, "/UserInformation.txt");
        try {
            FileOutputStream out = new FileOutputStream(newFile);
            ObjectOutputStream oos = new ObjectOutputStream(out);
            oos.writeObject(this);
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Getters and Setters for the user information
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getCurrentWeight() {
        return currentWeight;
    }

    public void setCurrentWeight(String currentWeight) {
        this.currentWeight = currentWeight;
    }

    public String getTargetWeight() {
        return targetWeight;
    }

    public void setTargetWeight(String targetWeight) {
        this.targetWeight = targetWeight;
    }

    public long getConcentration() {
        return concentration;
    }

    public void setConcentration(int concentration) {
        this.concentration = concentration;
    }
}
