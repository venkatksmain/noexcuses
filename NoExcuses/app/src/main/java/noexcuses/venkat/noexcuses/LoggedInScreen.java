package noexcuses.venkat.noexcuses;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;


public class LoggedInScreen extends ActionBarActivity {
    private String ActiveUser = "empty";
    private UserInformation ActiveUserInformation = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logged_in_screen);
        Intent intent = getIntent();
        ActiveUser = intent.getStringExtra("username"); //if it's a string you stored.
        ActiveUserInformation = UserInformation.readFromFile(getApplicationContext(), ActiveUser);
        TextView usernameView = (TextView) findViewById(R.id.userNameView);
        usernameView.setText("Currently Logged In As: "+ActiveUser);
        TextView name = (TextView) findViewById(R.id.nameView2);
        name.setText("Welcome, "+ ActiveUserInformation.getName()+"!");


        //ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
        double progress1 = (Double.valueOf(ActiveUserInformation.getCurrentWeight()) / Double.valueOf(ActiveUserInformation.getTargetWeight()))*100;
        double progress2 = (Double.valueOf(ActiveUserInformation.getTargetWeight()) / Double.valueOf(ActiveUserInformation.getCurrentWeight()))*100;
        double progress = min(progress1,progress2);
        //progressBar.setProgress((int) progress);
        TextView weightView  = (TextView) findViewById(R.id.weightView);
        weightView.setText("You're "+(int)progress+"% of the way to your weight goal!");
    }

    double min(Double a, Double b){
        if(a<b) {
            return a;
        }
        else if (a>b){
            return b;
        }
        return a;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_logged_in_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
